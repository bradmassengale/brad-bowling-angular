import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ScoresService } from '../scores.service';
import { ScoreboardComponent } from './scoreboard.component';

describe('ScoreboardComponent', () => {
  let component: ScoreboardComponent;
  let fixture: ComponentFixture<ScoreboardComponent>;
  let scoresService: jasmine.SpyObj<ScoresService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScoreboardComponent ],
      providers: [  {
        provide: ScoresService,
        useValue: jasmine.createSpyObj('ScoresService', ['addSymbol'])
        } ]
    })
    .compileComponents().then(() => {
      fixture = TestBed.createComponent(ScoreboardComponent);
      component = fixture.componentInstance;
      scoresService = TestBed.get(ScoresService);
      scoresService.scores = [ '', '', '', ];
    });
  }));

  beforeEach(() => {
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
