import { Component, DoCheck } from '@angular/core';
import { ScoresService } from '../scores.service';

@Component({
  selector: 'app-scoreboard',
  templateUrl: './scoreboard.component.html',
  styleUrls: ['./scoreboard.component.scss']
})

export class ScoreboardComponent implements DoCheck {
  public total: number;
  constructor( public scoresService: ScoresService ) { }

  ngDoCheck() {
    if (this.scoresService.index >= 19 ) {
      this.total = this.scoresService.getTotal();
    }
    if (this.scoresService.index === 0) {
      this.total = null;
    }
  }
}
