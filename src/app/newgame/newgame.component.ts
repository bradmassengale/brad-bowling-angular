import { Component } from '@angular/core';
import { ScoresService } from '../scores.service';

@Component({
  selector: 'app-newgame',
  templateUrl: './newgame.component.html',
  styleUrls: ['./newgame.component.scss']
})
export class NewgameComponent {
  public scoreHistory: number[] = [];
  constructor(public scoresService: ScoresService) { }

  public newGame(): void {
    this.scoreHistory.push(this.scoresService.getTotal());
    this.scoresService.index = 0;
    this.scoresService.scores = [ '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];
    this.scoresService.buttonDisabled = false;
    this.scoresService.rollsRemaining = 20;
  }
}
