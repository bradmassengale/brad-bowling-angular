import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ScoresService } from '../scores.service';
import { NewgameComponent } from './newgame.component';

describe('NewgameComponent', () => {
  let componentInstance: NewgameComponent;
  let fixture: ComponentFixture<NewgameComponent>;
  let scoresService: jasmine.SpyObj<ScoresService>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewgameComponent ],
      providers: [ {
          provide: ScoresService,
          useValue: jasmine.createSpyObj('ScoresService', ['getTotal'])
        }
      ]
    })
    .compileComponents().then(() => {
      fixture = TestBed.createComponent(NewgameComponent);
      componentInstance = fixture.componentInstance;
      scoresService = TestBed.get(ScoresService);
      scoresService.getTotal.and.callFake(() => 87 );
       });
      }));

  beforeEach(() => {
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(componentInstance).toBeTruthy();
  });

  it('should add the previous score to the log', () => {
    const expected = 87;
    componentInstance.newGame();
    const result = componentInstance.scoreHistory[0];
    expect(result).toEqual(expected);
  });
});
