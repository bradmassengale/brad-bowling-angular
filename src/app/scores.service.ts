import { Injectable } from '@angular/core';

@Injectable()
export class ScoresService {
  public scores: string[] = [ '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];
  public index = 0;
  public buttonDisabled = false;
  public rollsRemaining = 20;
  constructor() { }

  public addSymbol(index: number, input: string): void {
    this.scores.splice(index, 0, input);
  }

  private symbolToNumberConversion(): number[] {
    const array: number[] = [];
    this.scores.forEach( (score) => {
      if (score === 'X') {
        array.push(10);
      } else if (score === '-') {
        array.push(0);
      } else if ( score === '/') {
        array.pop();
        array.push(0);
        array.push(-100000);
      } else if ( score === ' ') {
      } else {
        array.push(Number(score));
      }
    });
    return array;
  }

  public getTotal(): number {
    const convertedArray = this.symbolToNumberConversion();
    let total = 0;

    for ( let index = convertedArray.length - 1; index >= 0; index--) {
      if (index === convertedArray.length - 1) {
        if (convertedArray[index] === 10) {
          total += 0;
          continue;
        }
      }
      if (index === convertedArray.length - 2) {
        if (convertedArray[index] === 10) {
          total += 0;
          continue;
        }
      }
      if (index === convertedArray.length - 3) {
        if (convertedArray[index] === 10) {
          total += (10 + (convertedArray[index + 1]) + (convertedArray[index + 2]));
          continue;
        }
      }

      if (convertedArray[index] === -100000) {
        total += (10 + (convertedArray[index + 1]));
      } else if (convertedArray[index] === 10) {
          total += (10 + (convertedArray[index + 1]) + (convertedArray[index + 2]));
        } else {
            total += convertedArray[index];
      }
    }
    return total;
  }
}
