import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RollgeneratorComponent } from './rollgenerator/rollgenerator.component';
import { ScoresService } from './scores.service';
import { ScoreboardComponent } from './scoreboard/scoreboard.component';
import { NewgameComponent } from './newgame/newgame.component';

@NgModule({
  declarations: [
    AppComponent,
    RollgeneratorComponent,
    ScoreboardComponent,
    NewgameComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [ScoresService],
  bootstrap: [AppComponent]
})
export class AppModule { }
