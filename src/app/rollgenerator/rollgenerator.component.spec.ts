import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RollgeneratorComponent } from './rollgenerator.component';
import { ScoresService } from '../scores.service';
import { By } from '@angular/platform-browser';


describe('RollgeneratorComponent', () => {
  let componentInstance: RollgeneratorComponent;
  let fixture: ComponentFixture<RollgeneratorComponent>;
  let scoresService: jasmine.SpyObj<ScoresService>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RollgeneratorComponent ],
      providers: [  {
        provide: ScoresService,
        useValue: jasmine.createSpyObj('ScoresService', ['addSymbol'])
        } ]
    })
    .compileComponents().then(() => {
      fixture = TestBed.createComponent(RollgeneratorComponent);
      componentInstance = fixture.debugElement.componentInstance;
      scoresService = TestBed.get(ScoresService);
      scoresService.scores = [ '', '', '', ];
      scoresService.addSymbol.and.callFake( (index, input) => {
        scoresService.scores.splice(index, 0, input);
       } );
      });
    }));
  beforeEach(() => {
    fixture.detectChanges();
    componentInstance.currentRoll = 0;
    scoresService.index = 0;
  });

  it('should generate a number between 0 and 10', () => {
    const result = componentInstance.generateRoll();
    expect(result >= 0 && result <= 10).toBeTruthy();
  });

  it('should never generate a number greater than 10 per frame', () => {
    componentInstance.onClick();
    componentInstance.onClick();
    const result = componentInstance.totalScoreForFrame;
    expect(result).toBeLessThanOrEqual(10);
  });

  it('should reset the frame score if the previous roll was 10', () => {
    componentInstance.currentRoll = 10;
    const expected = 0;
    componentInstance.checkCurrentFrame();
    const result = componentInstance.totalScoreForFrame;
    expect(result).toBe(expected);
  });

  it('should reset the frame score if the frame is an odd number', () => {
    scoresService.index = 7;
    componentInstance.onClick();
    const expected = componentInstance.totalScoreForFrame;
    componentInstance.checkCurrentFrame();
    const result = componentInstance.totalScoreForFrame;
    expect(result).toBe(expected);
  });

  it('should pass an X if the user "bowls" a strike', () => {
    componentInstance.currentRoll = 10;
    componentInstance.pushScore();
    const result = scoresService.scores[0];
    expect(result).toBe('10');
  });

  it('should pass a blank as the second value if the user "bowls" a strike', () => {
    componentInstance.currentRoll = 10;
    componentInstance.pushScore();
    const result = scoresService.scores[1];
    expect(result).toBe('');
  });

  it('should allow another roll if the 10th frame has a strike ', () => {
    const expected = 19;
    scoresService.index = 18;
    componentInstance.currentRoll = 10;
    componentInstance.onClick();
    const result = scoresService.index;
    expect(result).toEqual(expected);
  });

  it('should allow another roll if the 10th frame has a spare ', () => {
    const button = fixture.debugElement.query(By.css('.bowlButton')).nativeElement;
    scoresService.index = 19 ;
    componentInstance.totalScoreForFrame = 10;
    scoresService.rollsRemaining = 0;
    componentInstance.isButtonDisabled();
    expect(componentInstance.scoresService.buttonDisabled).toBeFalsy();
  });

  it('should not allow another roll if the 10th frame has a score less than 10 ', () => {
    spyOn(componentInstance, 'onClick');
    const button = fixture.debugElement.query(By.css('.bowlButton')).nativeElement;
    scoresService.index = 20 ;
    componentInstance.totalScoreForFrame = 9;
    scoresService.rollsRemaining = 0;
    componentInstance.isButtonDisabled();
    fixture.detectChanges();
    button.click();
    expect(componentInstance.onClick).not.toHaveBeenCalled();
  });
});
