import { Component, Output } from '@angular/core';
import { ScoresService } from '../scores.service';

@Component({
  selector: 'app-rollgenerator',
  templateUrl: './rollgenerator.component.html',
  styleUrls: ['./rollgenerator.component.scss']
})

export class RollgeneratorComponent {
  public totalScoreForFrame = 0;
  public maxRoll = 11;
  public currentRoll: number;
  public tenthFrameSpare = false;
  constructor(public scoresService: ScoresService) { }

  public generateRoll(): number {
    return Math.floor(Math.random() * this.maxRoll);
  }

  public checkCurrentFrame(): void {
    if ((this.scoresService.rollsRemaining) % 2 === 0) {
      this.resetStats();
    } else if (this.totalScoreForFrame <= 10 && (this.scoresService.rollsRemaining) % 2 !== 0) {
        this.maxRoll -= this.currentRoll;
    }
  }

  public resetStats(): void {
    this.totalScoreForFrame = 0;
    this.maxRoll = 11;
  }

  public pushScore(): void {
    if (this.scoresService.index > 17 && this.currentRoll === 10) {
      this.scoresService.addSymbol(this.scoresService.index, 'X');
    } else if (this.currentRoll === 10 && ((this.scoresService.rollsRemaining) % 2 === 0)) {
        this.scoresService.addSymbol(this.scoresService.index, 'X');
        if (this.scoresService.index !== 20) {
          this.scoresService.index++;
        }
        this.scoresService.rollsRemaining--;
        this.scoresService.addSymbol(this.scoresService.index, ' ');
      } else if (this.currentRoll === 0) {
          this.scoresService.addSymbol(this.scoresService.index, '-');
      } else if ( this.totalScoreForFrame === 10 ) {
          this.scoresService.addSymbol(this.scoresService.index, '/');
      } else {
        this.scoresService.addSymbol(this.scoresService.index, this.currentRoll.toString());
    }
  }

  public doesTenthFrameHaveSpare(): void {
    if ( this.scoresService.index === 20 && this.totalScoreForFrame === 10) {
      this.scoresService.rollsRemaining++;
      this.tenthFrameSpare = true;

    }
  }

  public doesTenthFrameHaveStrike() {
    if (this.scoresService.index === 19 && this.totalScoreForFrame === 10) {
      this.scoresService.rollsRemaining += 2;
    } else if (this.scoresService.index > 19 && this.totalScoreForFrame === 10) {
        this.scoresService.rollsRemaining++;
    }
  }

  public isButtonDisabled() {
    if ( this.scoresService.index > 19 && this.totalScoreForFrame !== 10) {
      this.scoresService.buttonDisabled = true;
    } else if (this.scoresService.index === 21) {
      this.scoresService.buttonDisabled = true;
    }
  }

  public onClick(): void {
    this.checkCurrentFrame();
    if (this.totalScoreForFrame !== 10) {
      this.currentRoll = this.generateRoll();
      this.totalScoreForFrame += this.currentRoll;
      this.pushScore();
    }
    this.scoresService.index++;
    this.scoresService.rollsRemaining--;
    if (this.scoresService.index >=  19) {
        this.doesTenthFrameHaveSpare();
        this.doesTenthFrameHaveStrike();
        this.isButtonDisabled();
    }
  }
}
