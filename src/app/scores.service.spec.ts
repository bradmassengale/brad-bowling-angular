import { TestBed } from '@angular/core/testing';

import { ScoresService } from './scores.service';

describe('ScoresService', () => {
  let service: ScoresService;
  beforeEach(() => TestBed.configureTestingModule({ providers: [ScoresService] })
  .compileComponents().then(() => {
    service = TestBed.get(ScoresService);
  }));

  it('should replace provided index with provided input', () => {
    const expected = 'input';
    service.addSymbol(0, 'input');
    const result = service.scores[0];
    expect(result).toEqual('input');
  });

  it('should calculate a game with no spares or strikes - like whenever I play with no bumpers', () => {
    service.scores = [ '1', '2', '3', '4', '5', '3', '7', '1', '9', '-', '1', '2', '3', '4', '5', '1', '7', '1', '9', '-', ' '];
    const expected = 68;
    const result = service.getTotal();
    expect(result).toEqual(expected);
  });

  it('should calculate a game with a spare in the last frame', () => {
    service.scores = [ '1', '2', '3', '4', '5', '3', '7', '1', '9', '-', '1', '2', '3', '4', '5', '1', '7', '1', '9', '/', '2'];
    const expected = 73;
    const result = service.getTotal();
    expect(result).toEqual(expected);
  });

  it('should calculate a game with strikes and spares scattered throughout', () => {

    service.scores = [ '1', '/', '3', '4', 'X', '1', '7', '1', '9', '-', '1', '2', '3', '4', '5', '1', '7', '1', '9', '/', '2'];
    const expected = 94;
    const result = service.getTotal();
    expect(result).toEqual(expected);
  });

  it('should calculate a perfect game', () => {
    service.scores = [ 'X', ' ', 'X', ' ', 'X', ' ', 'X', ' ', 'X', ' ', 'X', ' ', 'X', ' ', 'X', ' ', 'X', ' ', 'X', 'X', 'X'];
    const expected = 300;
    const result = service.getTotal();
    expect(result).toEqual(expected);
  });
});
